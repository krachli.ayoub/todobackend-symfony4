<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Todo items
 * 
 * @ORM\Entity(repositoryClass="App\Repository\TodoRepository")
 * 
 * We add a custom DELETE operation (see https://api-platform.com/docs/core/operations#creating-custom-operations-and-controllers)
 * @ApiResource(collectionOperations={
 *     "get",
 *     "post",
 *     "delete"={
 *         "method"="DELETE",
 *         "path"="/todos",
 *         "controller"="App\Controller\TodoDelete",
 *         "swagger_context"={
 *              "summary"="Removes all Todo resources",
 *              "parameters"={
 *              }
 *         }
 *     }
 *  },
 *  itemOperations={
 *          "get",
 *          "patch"={
 *              "method"="PATCH"
 *              },
 *          "delete"
 *       })
 */

class Todo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="boolean")
     */
    private $completed = false;

    /**
     * 
     * Renamed 'order' property : "order" is reserved
     * we nevertheless provide getters and setters of order below
     * @ORM\Column(type="integer")
     */
    private $todo_order = 0;

    //    public $url = '';
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCompleted(): ?bool
    {
        return $this->completed;
    }

    public function setCompleted(bool $completed): self
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * getter for 'order'
     * @return int|NULL
     */
    public function getOrder(): ?int
    {
        return $this->todo_order;
    }

    /**
     * setter for 'order'
     * @param int $order
     * @return self
     */
    public function setOrder(int $order): self
    {
        $this->todo_order = $order;

        return $this;
    }
    
}
