<?php
// src/Swagger/SwaggerDecorator.php

namespace App\Swagger;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class SwaggerDecorator implements NormalizerInterface
{
    private $decorated;
    
    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }
    
    public function normalize($object, $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);
        
        // Override title
        $docs['info']['title'] = 'Example Todo-Backend API implementation with Symfony 4 and api-platform';
        
        $docs['info']['description'] = 'This is an example of the <a href="https://www.todobackend.com/">Todo-Backend</a> API implementation with <a href="https://symfony.com/">Symfony 4</a> and <a href="https://api-platform.com/">api-platform</a>.'
                                       . "<br>"
                                       . '<br>To be tested at <a href="http://www.todobackend.com/specs/index.html?https://still-ravine-70063.herokuapp.com/todos">http://www.todobackend.com/specs/index.html?https://still-ravine-70063.herokuapp.com/todos</a>'
                                       . "<br>"
                                       . "<br>Copyright (c) 2018 Olivier Berger and Télécom SudParis, released as Free Software under an MIT/X license.";
        
        return $docs;
    }
    
    public function supportsNormalization($data, $format = null)
    {
        return $this->decorated->supportsNormalization($data, $format);
    }
}